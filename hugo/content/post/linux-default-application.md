+++
categories = ["linux"]
date = "2014-09-22T08:30:55+08:00"
description = "在 Linux 下預設程式是利用 MIME Type 來判斷的 ... ..."
tags = ["gui", "markdown"]
title = "Linux 預設程式"

+++

在 Linux 下預設程式 (Default / Preferred Application) 是利用 MIME Type 來判斷的。MIME Type 及 Desktop Entry 的配對是由 `mimeapps.list` 這個檔案來判斷的。而這個檔案的位置有一連串的 lookup path，可以參考 [ArchWiki](https://wiki.archlinux.org/index.php/default_applications#MIME_types_and_desktop_entries)。以 Lubuntu 14.04 爲例，存在 `$HOME/.config/mimeapps.list` 這個檔案，將檔案相對應的 MIME Type 與程式的 Desktop Entry 連結即可。

值得注意的是，在 Terminal 下常用來開桌面程式的指令 `xdg-open` 讀取的位置爲 `$HOME/.local/share/applications/mimeapps.list` 而非按照標準順序尋找。因此若是需要可以用 `xdg-open` 來開啓，記得將連結指定在此位置。

若是檔案沒有標準的 MIME Type ，可以自定 xml 來設定及安裝新的 MIME Type。下面以 Firefox 作爲 Markdown 的預設開啓爲例，並假設系統沒有 Markdown 的 MIME Type （過去 Markdown 並沒有標準的 MIME Type，但是現今包括 Ubuntu 在內的系統都已有 `x-markdown`）。

    
創造 MIME Type 檔 `$HOME/.local/share/mime/packages/x-markdown.xml`

{{% highlight xml %}}
    <?xml version="1.0" encoding="UTF-8"?>
    <mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
      <mime-type type="text/x-web-markdown">
        <sub-class-of type="text/plain"/>
        <comment>Markdown File</comment>
        <glob pattern="*.md"/>
        <glob pattern="*.mkd"/>
        <glob pattern="*.markdown"/>
      </mime-type>
    </mime-info>
{{% /highlight %}}



安裝 x-markdown MIME Type

    > xdg-mime install --novendor $HOME/.local/share/mime/packages/x-markdown.xml


修改 `$HOME/.local/share/applications/mimeapps.list` 關聯 MIME Type 與 Firefox

    [Default Applications]
    ...
    text/x-markdown=firefox.desktop

    [Added Associations]
    ...
    text/x-markdown=firefox.desktop
