+++
categories = ["程式設計"]
date = "2014-09-30T14:13:45+08:00"
description = "Go 的 Type system 與 C++ 系起初看起來相似，但是實際使用發現其中概念完全不同..."
tags = ["golang"]
title = "Golang Type System"
+++

Go 的 Type system 與 C++ 系起初看起來相似，但是實際使用發現其中概念完全不同。一般物件導向程式是集中式的 - 所有的 Member variable、Method、實作的 Interface 都宣告於一個 Class 當中；而 Go 的 Struct 則接近於 C 的 Struct，只擁有「資料]。其 Method 更像是傳入特定型態作爲參數的 Function，而任何實作 Interface 所需求 Function 的 Struct 都可以當作該 Interface。

相對於 Python Dynamic type 無法有 Type checking 及 Java 需要進入類別去新增 Method、實作 Interface，Go 的好處是能夠在任何需要時定義新的 Mtehod 且自動成爲 Interface 的一員。毫無疑問這也是 Go 這個 Static type language 寫起來很像 Dynamic type 的原因之一。

以下以一般物件導向的面向來看 Go 的不同點：

<h3>封裝</h3>
<h4>Access Privilege</h4>
Go 的 Access Privilege 非常的單純，只有一種 - Package。他相信在同一個 Package 內的開發者知道自己在做什麼！而 export 的方式則與 Python 類似用 Naming。Python 是以底線開頭爲 Private；Go 則是以開頭小寫爲 Private，大寫爲 Public。

<h4>Method</h4>
如同前述，Method 比較類似傳入特定型態的 Function。因此 `func (t Type) f()` 和 `func (tp *Type) f()` 是不同的兩個 Method，此外也能替非自定 Type，比如 Slice，來定義 Method `func （s []Type) f()`。但是同時 Method 也能帶來和 Type instance 自動綁定和避免命名空間污染的好處，使用便利性上與其他程式無異。


<h3>繼承</h3>
Go 並沒有傳統的繼承，而是使用類似 Composition + Delegation 的 Embedding 來達成。Embedding 就像是擁有一個與 Member type 同名稱的 Member，但是同時會把 Member 底下的 Member variable / Method 全部提升到自身。

{{% highlight go %}}
    package main

    import (
        "fmt"
    )

    type OS struct {
        name string
    }

    func (self *OS) GetName() string {
        return self.name
    }

    type Phone struct {
        *OS
        Menufacturer string
    }

    func main() {
        OneM7 := &Phone{OS: &OS{"Android"}, Menufacturer: "HTC"}
        fmt.Println(OneM7.GetName())
    }
{{% /highlight %}}


若定義一個 Member 擁有的名字會覆蓋掉 Delegation， 當有 Collision 或是需要自定並呼叫 Member 原本的 Variable / Method 時可以定義一個新版本並直接 call full name。

{{% highlight go %}}
    package main

    import (
        "fmt"
    )

    type OS struct {
        name string
    }

    func (self *OS) GetName() string {
        return self.name
    }

    type CPU struct {
        name string
    }

    func (self *CPU) GetName() string {
        return self.name
    }

    type Phone struct {
        *OS
        *CPU
        name         string
        Menufacturer string
    }

    func (self *Phone) GetName() string {
        return self.name + ": " + self.OS.GetName() + " with " + self.CPU.GetName()
    }

    func main() {
        OneM7 := &Phone{
            OS:           &OS{"Android"},
            CPU:          &CPU{"Snapdragon600"},
            name:         "HTC ONE M7",
            Menufacturer: "HTC"}
        fmt.Println(OneM7.GetName())
    }
{{% /highlight %}}


<h3>多型</h3>
Go 的多型完全倚賴 Interface。如同一開始提到，只要能滿足 Interface 的需求，即可轉型成該 Interface type。值得注意的是 Embedding 所表達的 階層關係與其他程式語言的繼承不同，並不能當作多型的依據。而 Interface 同樣可以使用 Embedding 來嵌入其他的 Interface，但是 Collision 就無法可解。只能 Refactoring 或是自己定義一個 Interface。

參考資料：  
1. http://golang.org/doc/effective_go.html#embedding
