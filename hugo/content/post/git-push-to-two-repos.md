+++
categories = ["軟體工具"]
date = "2014-09-23T02:44:36+08:00"
description = "在使用 Git 有時會需要同時 push 至兩個 Repo。以我來說，通常會將個人資料同步在 Github 及 Bitbucket ... ..."
tags = ["git"]
title = "Git 同時 push 至兩個 Repo"

+++

在使用 Git 有時會需要同時 push 至兩個 Repo。以我來說，通常會將個人資料同步在 [Github](https://github.com/carylorrk) 及 [Bitbucket](https://bitbucket.org/carylorrk) 上作爲備份。利用 `git remote` 這個指令可以增修 remote repo 相關的參數。當要新增一個 Push URL 至某個 Remote Name 下，可以如此操作：`git remote set-url --add --push <name> <url>`。

在這裏要特別注意的是，這個指令會增加一個 Push URL 並取代預設尋找的位置（就是你在 remote add 時給的 URL），所以如果要同時 Push 至兩個 Repo 需要將原本的 URL 也加上去。以下例子可以看出來：

    > git remote -v
    origin  git@github.com:carylorrk/two_repo (fetch)
    origin  git@github.com:carylorrk/two_repo (push)

    > vim .git/config
    ...
    [remote "origin"]
        url = git@github.com:carylorrk/two_repo
        fetch = +refs/heads/*:refs/remotes/origin/*

`> git remote set-url --add --push origin git@bitbucket.org:carylorrk/two_repo`  

    > git remote -v
    origin  git@github.com:carylorrk/two_repo (fetch)
    origin  git@bitbucket.org:carylorrk/two_repo (push)

    > vim .git/config
    ...
    [remote "origin"]
        url = git@github.com:carylorrk/two_repo
        fetch = +refs/heads/*:refs/remotes/origin/*
        pushurl = git@bitbucket.org:carylorrk/two_repo

`> git remote set-url --add --push origin git@github.com:carylorrk/two_repo`  

    > git remote -v
    origin  git@github.com:carylorrk/two_repo (fetch)
    origin  git@bitbucket.org:carylorrk/two_repo (push)
    origin  git@github.com:carylorrk/two_repo (push)

    > vim .git/config
    ...
    [remote "origin"]
        url = git@github.com:carylorrk/two_repo
        fetch = +refs/heads/*:refs/remotes/origin/*
        pushurl = git@bitbucket.org:carylorrk/two_repo
        pushurl = git@github.com:carylorrk/two_repo

參考資料:
1. http://stackoverflow.com/questions/14290113/git-pushing-code-to-two-remotes
