+++
date = "2014-09-22T05:05:24+08:00"
description = "曾學習過的程式語言、工具、領域 ... ..."
title = "技能樹"
+++

星號代表熟悉度，三顆星代表有基礎瞭解，不滿則代表曾使用過但沒有整體概念。


Programming Language
--------------------

* C/C++ `*****`
    * C++11 `****`
    * Metaprogramming `***`
    * Boost(including unit test) `***`
* Shell Script `****`
* Python `****` 
* Go `***`
* Javascript `***`
* Java `***`
* PHP `**`


Tool
--------
* Vim `*****`
* Version Controll
    * SVN `****`
    * Git `****`
* Build System
    * Makefile `*****`
    * CMake `***`
    * Ant `**`
* GCC `*****`
* GDB `****`


Domain Knowldge
---------------
* Linux
    * Admin `*****`
    * Kernel / Module `***`
* Web Design
    * Django (server side) `****`
    * Bootstrap (client side style) `****`
    * AngularJS (client side dynamic) `*****`
    * Hugo (static site generator) `*****`
* Mobile App
    * Android `***`
    * Cordova (PhoneGap) `*****`
* Virtualization
    * Binary Translation `*****`
    * System Virtualization `***`
    * QEMU `*****`
    * LLVM `**`
* Distribution System
    * Hadoop (computing and storage scalability) `****`
    * Auto-balance / Auto-scaling `***`
* Embedded System `***`
* Data Mining `***`
